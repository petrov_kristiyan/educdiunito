package com.example.educdiunito;

import java.io.IOException;
import java.util.LinkedList;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class RSSFeedParser {

    private XmlPullParser xmlParser;
    private int eventType;
    private String ns=null;

    public RSSFeedParser(XmlPullParser xmlParser) throws XmlPullParserException, IOException {
        this.xmlParser = xmlParser;
        move();
    }

    void move() throws XmlPullParserException, IOException {
        eventType = xmlParser.next();
    }

    void matchStart(String t) throws XmlPullParserException, IOException {
        xmlParser.require(XmlPullParser.START_TAG, ns, t);
    }

    void matchEnd(String t) throws XmlPullParserException, IOException {
        xmlParser.require(XmlPullParser.END_TAG, ns, t);
    }

    private void skip() throws XmlPullParserException, IOException {
        if (eventType != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        move();
        while (depth != 0) {
            switch (eventType) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
            move();
        }
    }

    private String readText() throws IOException, XmlPullParserException {
        String result = "";
        if (xmlParser.next() == XmlPullParser.TEXT) {
            result = xmlParser.getText();
        }
        return result;
    }

    private String link() throws IOException, XmlPullParserException {
        matchStart(Tag.link);
        String link = readText();
        move();
        matchEnd(Tag.link);
        return link;
    }

    private String title() throws IOException, XmlPullParserException {
        matchStart(Tag.title);
        String title = readText();
        move();
        matchEnd(Tag.title);
        return title;

    }

    private String description() throws IOException, XmlPullParserException {
        matchStart(Tag.description);
        String description = readText();
        move();
        matchEnd(Tag.description);
        return description;

    }

    private String pubDate() throws IOException, XmlPullParserException {
        matchStart(Tag.pubDate);
        String pubDate = readText();
        move();
        matchEnd(Tag.pubDate);
        return pubDate;

    }

    private String Guid() throws IOException, XmlPullParserException {
        matchStart(Tag.guid);
        String guid = readText();
        move();
        matchEnd(Tag.guid);

        return guid;

    }

    private String author() throws IOException, XmlPullParserException {
        matchStart(Tag.author);
        String author = readText();
        move();
        matchEnd(Tag.author);
        return author;
    }

    private String language() throws IOException, XmlPullParserException {
        matchStart(Tag.language);
        String language = readText();
        move();
        matchEnd(Tag.language);
        return language;
    }

    private String copyright() throws IOException, XmlPullParserException {
        matchStart(Tag.copyright);
        String copyright = readText();
        move();
        matchEnd(Tag.copyright);
        return copyright;
    }

    private Feed Channel() throws IOException, XmlPullParserException {
    	matchStart(Tag.channel);
    	Feed feed= new Feed();
        String title="";
        String link="";
        String description="";
        String pubDate="";
        String copyright="";
        String language="";
        String author="";
        LinkedList<FeedMessage> feedMessage = new LinkedList<FeedMessage>();
        move();
        while (eventType != XmlPullParser.END_TAG) {
            if (xmlParser.getEventType() != XmlPullParser.START_TAG) {
                move();
                continue;
            }
            String name = xmlParser.getName();
            	if(name.equals(Tag.title))
                     title = title();
           		else  
                   if(name.equals(Tag.link))
                     link=link();
                else
                     if(name.equals(Tag.description))
                    	 description=description();
                  else
                    	 if(name.equals(Tag.pubDate))
                    		 pubDate=pubDate();
                     else
                
                     if(name.equals(Tag.item))
                    	 feedMessage.add(item());

                     else
                     if(name.equals(Tag.copyright))
                     copyright=copyright();
                     else
                 
                     if(name.equals(Tag.language))
                     language=language();

                     else
                     if(name.equals(Tag.author))
                    	 author=author();
                     else
                    	 skip();
            
           	feed=new Feed(title,link,language,pubDate,description,copyright,author,feedMessage);
           	move();
        }
        matchEnd(Tag.channel);
        return feed;

    }

    public Feed rss() throws IOException, XmlPullParserException {  
    	  matchStart(Tag.rss); 
    	  eventType = xmlParser.nextTag();
          Feed feed = Channel();
          eventType = xmlParser.nextTag();
          matchEnd(Tag.rss);
          return feed;
    }

    private FeedMessage item() throws IOException, XmlPullParserException {
        FeedMessage feedMessage = new FeedMessage();
        matchStart(Tag.item);
        move();
        while (eventType != XmlPullParser.END_TAG) {
            if (xmlParser.getEventType() != XmlPullParser.START_TAG) {
                move();
                continue;
            }
            String name = xmlParser.getName();

                if(name.equals(Tag.title))
                    feedMessage.setTitle(title());
                else
                    if(name.equals(Tag.pubDate))
                    feedMessage.setpubDate(pubDate());
                    else
                    if(name.equals(Tag.description))
                    feedMessage.setDescription(description());
                    else
                    if(name.equals(Tag.guid))
                    feedMessage.setGuid(Guid());
                    else
                    if(name.equals(Tag.link))
                    feedMessage.setLink(link());
                    else
                    skip();
            move();
        }
        matchEnd(Tag.item);
        return feedMessage;

    }
}