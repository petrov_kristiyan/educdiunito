package com.example.educdiunito;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class FeedToHTML implements FeedVisitor{
  
    private StringBuilder htmlString;
   private boolean summary;
   private boolean flag;
  
   public FeedToHTML(){
	this.htmlString=new StringBuilder();
	this.summary=false;
    this.flag=false;
   }

   public void setSummary(boolean shortened){
   	this.summary=shortened;
   }
 
    @Override
    public void visit(FeedMessage  feedMessage){
     	if(flag){
    		htmlString.append("<div style=\"border-radius:0.3em;  box-shadow: 10px 10px 5px #888; padding:10px; margin:15px 0;background-color:white; \" ><p>\n");
    		flag=false;
    	}
     	else{
    		htmlString.append("<div style=\"border-radius:0.3em;  box-shadow: 10px 10px 5px #888; padding:10px; margin:15px 0;background-color:white;\" ><p>\n");
    		flag=true;
    	}
        if(!feedMessage.getPubDate().equals(""))
    	   htmlString.append("<em>"+feedMessage.getPubDate()+"</em><br>\n");
    	htmlString.append("<a style=\"color:#3366FF; font: bold 1.3em Arial, serif; text-decoration:none;\" href='");
    	if(!feedMessage.getLink().equalsIgnoreCase(""))
    	   htmlString.append(feedMessage.getLink());
    	else
    	   htmlString.append(feedMessage.getGuid());
    	htmlString.append("'>"+feedMessage.getTitle()+"</a><br><br>\n");
    	if(!summary) htmlString.append(feedMessage.getDescription()+"\n");
    	htmlString.append("</p></div>\n");
     }

        @Override
     public void visit(Feed feed){
      	Calendar rightNow=Calendar.getInstance();
    	DateFormat formatter= new SimpleDateFormat("MMM dd h:mmaa");
    	htmlString.append("<meta http-equiv=\"content-type\" content=\"text/html; charset=ISO-8859-1\">\n");
    	htmlString.append("<body style=\"background-color:lightgray;\"><h3>"+feed.getTitle()+"</h3>\n");
    	htmlString.append("Today is: <em>"+
    		formatter.format(rightNow.getTime())+"</em>.<br>\n");
    	if(!feed.getPubDate().equals(""))
    		htmlString.append("<em>Publication date: "+feed.getPubDate()+"</em>\n");
     }
     public String getHTML(){
     	return htmlString.toString();
     }	
    }