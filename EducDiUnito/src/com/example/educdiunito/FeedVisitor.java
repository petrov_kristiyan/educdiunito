package com.example.educdiunito;

interface FeedVisitor {

    public void visit(FeedMessage feedMessage);

    public void visit(Feed feed);
    
}
