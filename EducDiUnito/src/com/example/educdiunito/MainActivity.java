package com.example.educdiunito;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;



import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.content.Context;
import android.content.res.Configuration;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebSettings.RenderPriority;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity {
	private static String news="http://wwwold.educ.di.unito.it/EduFeed/rssNews_20.xml";
	private static String learn ="http://informatica.i-learn.unito.it/rss/file.php/1/1/forum/1/rss.xml";
	private static String stage="http://wwwold.educ.di.unito.it/EduFeed/rssTesi_20.xml";
	private static String educ="http://www.educ.di.unito.it";
	private static String encode="";
    private DrawerLayout DrawerLayout;
    private ListView DrawerList;
    private ActionBarDrawerToggle DrawerToggle;
    public static WebView textView;
    private long lastPressedTime;
    private static final int PERIOD = 2000;
    private CharSequence DrawerTitle;
    private CharSequence Title;
    private String[] Titles;
    public static String []ListURLs={
    	educ,news,stage,learn
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Title = DrawerTitle = getTitle(); //titolo barra
        Titles = getResources().getStringArray(R.array.links_array); //array dei link contenuti dentro links_array
        DrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout); //layout drawer
        DrawerList = (ListView) findViewById(R.id.left_drawer); // lista dei link
        //ombra sul contenuto quando la barra viene aperta
        DrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        //aggiungo alla lista i link e i listener
        DrawerList.setAdapter(new ArrayAdapter<String>(this, R.layout.drawer_list_item, Titles));
        DrawerList.setOnItemClickListener(new DrawerItemClickListener());

        //al click sulla barra mostra lo slider nascosto
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
       // getActionBar().setHomeButtonEnabled(true);

        // ActionBarDrawerToggle combina assieme il click dsull icona e lo sliding
        DrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                DrawerLayout,         /* DrawerLayout object */
                R.drawable.ic_drawer,  /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
                ) {
            public void onDrawerClosed(View view) {
                getSupportActionBar().setTitle(Title);
                supportInvalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
                getSupportActionBar().setTitle(DrawerTitle);
                supportInvalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        DrawerLayout.setDrawerListener(DrawerToggle);

        if (savedInstanceState == null) {
            selectItem(0);
        }
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
        	 if (event.getDownTime() - lastPressedTime < PERIOD){
                 finish();
             } else {
            	 if(textView.getOriginalUrl().equalsIgnoreCase(educ)||textView.getOriginalUrl().equalsIgnoreCase("about:blank")){
            		  Toast.makeText(getApplicationContext(), "Premi indietro ancora una volta per uscire.", Toast.LENGTH_SHORT).show();
 	                 lastPressedTime = event.getEventTime();
            	 } 
            	 else
            		 textView.goBack();  
             }
            return true;
        }
     return false;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
         // The action bar home/up action should open or close the drawer.
         // ActionBarDrawerToggle will take care of this.
        if (DrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle action buttons
        return super.onOptionsItemSelected(item);
    }

    /* The click listner for ListView in the navigation drawer */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }
    }

    private void selectItem(int position) {
       
    	 ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
         NetworkInfo ni = cm.getActiveNetworkInfo();
         if(ni!=null && ni.isConnected()){
	    	Fragment fragment = new RssFragment();
	        Bundle args = new Bundle();
	        args.putInt(RssFragment.LINKS, position);
	        fragment.setArguments(args);
	        FragmentManager fragmentManager = getSupportFragmentManager();
	        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
	        DrawerList.setItemChecked(position, true);
	        setTitle(Titles[position]);
	        DrawerLayout.closeDrawer(DrawerList);
	    }
         else
        	 Toast.makeText(getApplicationContext(), "Errore di connessione", Toast.LENGTH_SHORT).show(); 
    }

    @Override
    public void setTitle(CharSequence title) {
        Title = title;
        getSupportActionBar().setTitle(Title);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        DrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        DrawerToggle.onConfigurationChanged(newConfig);
    }
   

    /**
     * Fragment that appears in the "content_frame", shows a planet
     */
    
    public static class RssFragment extends Fragment {
    	

       public static final String LINKS = "link";
      
        public RssFragment() {
            // Empty constructor required for fragment subclasses
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_planet, container, false);
            int i = getArguments().getInt(LINKS);
            String planet = getResources().getStringArray(R.array.links_array)[i];
	        if(i!=0)
	            new DownloadFeed().execute(ListURLs[i]);
            else{
            	 textView =((WebView) rootView.findViewById(R.id.WebView1));
            	 textView.loadUrl(ListURLs[i]);
            	// enable javascript
            	 textView.getSettings().setJavaScriptEnabled(true);
             // load up the entire page
                 textView.getSettings().setLoadWithOverviewMode(true);
             // enable zoom
                 textView.getSettings().setBuiltInZoomControls(true);
             // enable zoom out
                 textView.getSettings().setUseWideViewPort(true);
             // enable to display flash contents
                 textView.getSettings().setPluginState(PluginState.ON);
                 
            	 textView.setWebViewClient(new WebViewClient() {
  	                public boolean shouldOverrideUrlLoading(WebView viewx, String urlx) {
  	                    viewx.loadUrl(urlx);
  	                    return false;
  	                }
  	            });
            }
	       
            textView =((WebView) rootView.findViewById(R.id.WebView1));
            textView.getSettings().setRenderPriority(RenderPriority.HIGH);
            textView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
            getActivity().setTitle(planet);
            return rootView;
        }
        public void putHtml(String result){
        	textView.loadDataWithBaseURL(null, result, "text/html",encode, null);
        }
        private class DownloadFeed extends AsyncTask<String,Void,String>{
    		@Override
    		protected String doInBackground(String... urls) {
    			try{
    				InputStream is = connectionFeed(urls[0]);
    				FeedToHTML f2h = new FeedToHTML();
    				Feed feed = downloadFeed(is);
    				feed.accept(f2h);
    				return f2h.getHTML();
    			}
    			catch(IOException e){
    				 return e.getMessage();
    			}catch(XmlPullParserException e){
    				return e.getMessage();
    			}
    		}
    		
    		@Override
    		protected void onPostExecute(String result){
    			if(result !=null)
    				putHtml(result);	
    		}

    		private Feed downloadFeed(InputStream is) throws XmlPullParserException, IOException {
    			 XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
    		        factory.setNamespaceAware(true);
    		        XmlPullParser xmlParser = factory.newPullParser();
    		        xmlParser.setInput(is,null);
    		        encode=xmlParser.getInputEncoding();
    		        RSSFeedParser parser = new RSSFeedParser(xmlParser);
    		        Feed feed = parser.rss();
    		        is.close();
    		        return feed;
    		}

    		private InputStream connectionFeed(String myurl) throws IOException {
    			InputStream is = null;
    			URL url = new URL(myurl);
    			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
    			conn.setReadTimeout(10000);
    			conn.setConnectTimeout(10000);
    			conn.setRequestMethod("GET");
    			conn.setDoInput(true);
    			conn.connect();
    			is = conn.getInputStream();
    			return is;
    		}
        	
        }
       }
}
