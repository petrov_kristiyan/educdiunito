package com.example.educdiunito;

import java.util.LinkedList;

public class Feed {
	
    private LinkedList<FeedMessage> items= new LinkedList<FeedMessage>();
    private String title;
    private String link;
	private String description;
    private String language;
    private String copyright;
    private String pubDate;
    private String author;
	
	public Feed(String title,String link,String language,String pubDate,String description,String copyright,String author,LinkedList<FeedMessage> items) {
    	this.title = title;
		this.link=link;
		this.description=description;
		this.language=language;
		this.copyright=copyright;
		this.pubDate=pubDate;
		this.items=items;
        this.author=author;         
	}
	public Feed() {
    	this.title = "";
		this.link="";
		this.description="";
		this.language="";
		this.copyright="";
		this.pubDate="";
        this.author="";         
	}
        public String getTitle() {
    		return title;
        }

	public String getLink() {
    		return link;
        }

       public String getDescription() {
                return description;
        }

       
        public String getLanguage() {
    		return language;
        }

	public String getCopyright() {
    		return copyright;
        }
        
        String getPubDate() {
                return pubDate;
        }
        
        public LinkedList<FeedMessage> getMessage(){
                return items;
        }

        public String toString(){
        	String s = "";
    		for (FeedMessage f : items) 
    			s += f;
    		return s;
        }

 	public void  accept(FeedVisitor visitor){
 		visitor.visit(this);
 		for (FeedMessage f : this.items )
     		f.accept(visitor);
	}
}
