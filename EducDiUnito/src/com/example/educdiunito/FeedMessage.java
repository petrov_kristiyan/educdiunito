package com.example.educdiunito;

    
public class FeedMessage{	
	
	private String title;
	private String description;
	private String link;
	private String author;
	private String pubDate;
	private String guid;
	
       
    public FeedMessage(){
    	this.title="";
		this.description="";
		this.author="";
		this.link="";
		this.pubDate="";
		this.guid="";
    }
	
	public void setTitle(String title){
		this.title=title;	
	}
	
	public void setDescription(String description){
		this.description=description;
	}
	
	public void setLink(String link){
		this.link=link;	
	}
	
	public void setAuthor(String author){
		this.author=author;	
	}

	public void setpubDate(String pubDate){
		this.pubDate=pubDate;	
	}
	public void setGuid(String guid){
		this.guid=guid;	
	}

	public String getTitle(){
		return title;
        }

	public String getDescription(){
		return description;
        }

	public String getLink(){
		return link;
        }

	public String getAuthor(){
		return author;
        }

	public String getPubDate(){
		return pubDate;
        }

	public String getGuid(){
		return guid;
        }
	
	public String toString(){
		return title+" "+description+ " "+link+" "+author+" "+pubDate+" "+guid+"\n";	
	}
	public void accept(FeedVisitor visitor){
            visitor.visit(this);
	}
}
