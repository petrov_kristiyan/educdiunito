package com.example.educdiunito;

public class Tag{
	public final static String rss="rss";
	public final static String channel="channel";
	public final static String title="title";
	public final static String link="link";
	public final static String description="description";
	public final static String pubDate="pubDate";
	public final static String item="item";
	public final static String guid="guid"; 
    public final static String author="author";
    public final static String language="language";
    public final static String copyright="copyright";

}